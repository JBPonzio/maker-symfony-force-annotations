Voici comment appliquer des patches à l'aide de `cweagans/composer-patches`.

## Installation composer-patches

Pour commencer, assurez-vous d'avoir Composer installé sur votre système. Ensuite, exécutez la commande suivante pour installer `cweagans/composer-patches` :

```bash
composer require cweagans/composer-patches
```
## Ajouter le patch
```json
"extra": {
    "patches": {
        "symfony/maker-bundle": {
            "Provide flag to force annotation in make entity command": "https://gitlab.com/api/v4/projects/48425625/repository/files/force-annotation-flag.patch/raw?ref=main"
        }
    }
}
```
## Appliquer le patch
```bash
composer update
```
